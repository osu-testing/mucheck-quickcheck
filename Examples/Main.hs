module Main where
import Test.QuickCheck
import Examples.QuickCheckTest

main = do
  quickCheckResult idEmpProp
  quickCheckResult revProp
  quickCheckResult modelProp
  return 0
